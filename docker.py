import psycopg2
from psycopg2 import Error
import requests
from datetime import datetime
import schedule

API_KEY = ' '#вставить  токен
# BASE_URL- Current API
BASE_URL = 'http://api.openweathermap.org/data/2.5/weather?'
# Saint _petersburg - 498817
CITY_ID = '498817'
def job():
    try:
        a = requests.get(BASE_URL, params={'id': CITY_ID, 'units': 'metric', 'lang': 'ru', 'APPID': API_KEY})
        b = a.json()
        curent_main = [
            (b['weather'][0]['main'],
             b['main']['temp'],
             b['main']['feels_like'],
             b['main']['humidity'],
             b['wind']['speed'],
             str(datetime.fromtimestamp(b['dt'])),
             b['name'],)]
        curent_second = [
            (b['name'],
            str(datetime.fromtimestamp(b['sys']['sunrise'])),
            str(datetime.fromtimestamp(b['sys']['sunset'])),)]
    except Exception as e:
        print("Error - :", e)
        pass

    try:
        # Подключение к существующей базе данных
        connection = psycopg2.connect(user=" ",
                                      # пароль и логин в environment, который указали при установке PostgreSQL
                                      password=" ",
                                      host="127.0.0.1",
                                      port="5432",
                                      database="grafana_t")

        # Курсор для выполнения операций с базой данных
        cursor = connection.cursor()
        query_main = """ INSERT INTO H_WEATHER
             (          MAIN,
                        CUR_TEMP,
                        CUR_TEMP_FEEL,
                        HUMIDITY,
                        WIND,
                        DATE_TIME,
                        COUNTRY)
                values 
                (%s,%s,%s,%s,%s,
                to_timestamp(%s,'YYYY-MM-DD HH24:MI:SS'),
                %s);"""
        # очистка стейджинга
        cursor.execute('DELETE FROM SUN_R_S_STG')
        # добавление в стейдинг новых записей
        query_second = """ INSERT INTO SUN_R_S_STG
             (      
                COUNTRY,
                SUNRISE_TIME,
                SUNSET_TIME)
                values 
                (%s,
                to_timestamp(%s,'YYYY-MM-DD HH24:MI:SS'),
                to_timestamp(%s,'YYYY-MM-DD HH24:MI:SS'))
                """
        cursor.executemany(query_main, curent_main)
        cursor.executemany(query_second, curent_second)

        cursor.execute('''INSERT INTO SUN_R_S
              (country,
                sunrise_time,
                sunset_time)
                SELECT
                * FROM SUN_R_S_STG
            WHERE NOT EXISTS
            (SELECT 1 FROM sun_r_s 
			 WHERE SUN_R_S.sunrise_time=SUN_R_S_STG.sunrise_time);
        ''')
        connection.commit()
        print("1 запись успешно вставлена")

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            print("Соединение с PostgreSQL закрыто")


schedule.every(60).minutes.do(job)



while True:
    schedule.run_pending()
